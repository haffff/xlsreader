﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XLSReader
{

    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
            ShopHourView.Items.Clear();

            ServerAdressTextBox.Text = Settings.Settings.items.ServerAdress;

            foreach (KeyValuePair<string,int> pair in Settings.Settings.items.Columns)
            {
                ColumnIndexesView.Items.Add(new KeyValuePair<string, string>(pair.Key , Parsers.IntToExcelColumn(pair.Value)));
            }
            foreach (KeyValuePair<int, int> pair in Settings.Settings.items.ShopColumnPairs)
            {
                ShopHourView.Items.Add(new KeyValuePair<string,string>(  Parsers.IntToExcelColumn(pair.Key),  Parsers.IntToExcelColumn(pair.Value)));
            }



        }

        bool stringHasOnlyLetters(string str)
        {
            foreach(char x in str)
            {
                if(!char.IsLetter(x))
                {
                    return false;
                }
            }
            return true;
        }


        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            InputWindows.SingleFieldInput window = new InputWindows.SingleFieldInput((result) => {
                if (!stringHasOnlyLetters(result.ToString()))
                {
                    // Handle
                    return;
                }
                var key = result.ToString();

                var x = new InputWindows.SingleFieldInput((result1) =>
                {
                    if(!stringHasOnlyLetters(result1.ToString()))
                    {
                        // Handle
                        return;
                    }
                    var value = result1.ToString();
                    ShopHourView.Items.Add(new KeyValuePair<string, string>(key, value));
                }, "Wprowadź Godziny");
                x.Show();
            }, "Wprowadź id sklepu");
            window.Show();
        }

        private void DelButton_Click(object sender, RoutedEventArgs e)
        {
            var item = ShopHourView.SelectedItem;
            if(item!=null)
            {
                ShopHourView.Items.Remove(item);
            }
        }

        private void ColumnIndexesView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ColumnIndexesView.SelectedItem;
            int index = ColumnIndexesView.SelectedIndex;

            InputWindows.SingleFieldInput window = new InputWindows.SingleFieldInput((result) => {
                if (!stringHasOnlyLetters(result.ToString()))
                {
                    // Handle
                    return;
                }
                ColumnIndexesView.Items.Remove(item);

                var key = ((KeyValuePair<string, string>)item).Key;
                var value = result.ToString();


                ColumnIndexesView.Items.Insert(index, new KeyValuePair<string, string>(key, value));
            });
            window.Show();
        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveQuitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var array = ColumnIndexesView.Items.Cast<KeyValuePair<string, string>>().ToArray();

                Settings.Settings.items.ServerAdress = ServerAdressTextBox.Text;

                Settings.Settings.items.Columns.Clear();

                foreach (var element in array)
                {
                    Settings.Settings.items.Columns.Add(element.Key, Parsers.ExcelColumnToInt(element.Value.Trim()));
                }

                array = ShopHourView.Items.Cast<KeyValuePair<string, string>>().ToArray();


                Settings.Settings.items.ShopColumnPairs.Clear();

                foreach (var element in array)
                {
                    Settings.Settings.items.ShopColumnPairs.Add(Parsers.ExcelColumnToInt(element.Key.Trim()), Parsers.ExcelColumnToInt(element.Value.Trim()));
                }

                Settings.Settings.Save();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Wystąpił błąd podczas zapisywnaia ustawień","Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
                Settings.Settings.Load();
            }
        }

    }
}
