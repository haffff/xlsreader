﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XLSReader
{
    public partial class ShowCourseDetails : Window
    {
        public ShowCourseDetails(Course course)
        {
            InitializeComponent();

            ListView.ItemsSource = course.ShopArrivePairs;
            this.Title = "Kurs:" + course.Id + " Kierowca:" + course.Driver;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
