﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace XLSReader.ImportModules
{
    class JSONImportModule : IImportModule
    {
        public ImportedCourses Import(string path)
        {
            try
            {
                return new ImportedCourses( JsonConvert.DeserializeObject<List<Course>>( File.ReadAllText( path ) ),null, false);
            }
            catch(Exception e)
            {
                MessageBox.Show("Podczas inportu wystąpił błąd", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return new ImportedCourses(null, null, true);
            }
        }


        async public Task<ImportedCourses> ImportAsync(string path)
        {
            return await Task.Run(() => Import(path));
        }
    }
}
