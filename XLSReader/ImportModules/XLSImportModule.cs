﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExcelDataReader;



namespace XLSReader.ImportModules
{
    using ShopArPair = KeyValuePair<int, DateTime>;


    public class XLSImportModule : IImportModule
    {
        public ImportedCourses Import(string path)
        {
            List<Course> suceffulCourses = new List<Course>();
            List<ErrorCourse> errorCourses = new List<ErrorCourse>();
            try
            {
                using (FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        int rowNumber = 0;


                        while (reader.Read())
                        {
                            rowNumber++;

                            var course = new Course();
                            try
                            {
                                course.Id = (int)reader.GetDouble(Settings.Settings.items.Columns["Id"]);
                                course.Driver = reader.GetString(Settings.Settings.items.Columns["Driver"]);
                                course.Truck = reader.GetString(Settings.Settings.items.Columns["Truck"]);
                                course.Ramp = (int)reader.GetDouble(Settings.Settings.items.Columns["Ramp"]);
                                course.Length = (int)reader.GetDouble(Settings.Settings.items.Columns["Length"]);

                                DateTime date = reader.GetDateTime(Settings.Settings.items.Columns["Day"]);

                                List<ShopArPair> shopList = new List<ShopArPair>();

                                foreach (KeyValuePair<int, int> pair in Settings.Settings.items.ShopColumnPairs)
                                {
                                    try
                                    {
                                        shopList.Add(new ShopArPair((int)reader.GetDouble(pair.Key), Parsers.SpanDateAndHours(date, reader.GetDateTime(pair.Value))));
                                    }
                                    catch
                                    {

                                    }
                                }

                                course.ShopArrivePairs = shopList.ToArray();

                                course.PreRouteDate = Parsers.SpanDateAndHours(date, reader.GetDateTime(Settings.Settings.items.Columns["PreRoute"]));
                                course.RouteStartDate = Parsers.SpanDateAndHours(date, reader.GetDateTime(Settings.Settings.items.Columns["RouteStart"]));
                                course.RouteEndDate = Parsers.SpanDateAndHours(date, reader.GetDateTime(Settings.Settings.items.Columns["RouteEnd"]));
                                suceffulCourses.Add(course);
                            }
                            catch (Exception e)
                            {
                                var errorc = new ErrorCourse();
                                errorc.Id = course.Id;
                                errorc.RowNumber = rowNumber;
                                errorc.Message = e.Message;
                                errorCourses.Add(errorc);
                            }
                        }
                    }
                }
            }
            catch (System.IO.IOException e)
            {
                MessageBox.Show("Nie można uzyskać dostępu do pliku, czy jest on otwarty w innnym programie?","Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
                return null;
            }
            catch (Exception e)
            {
                MessageBox.Show("Pojawił się nieoczekiwany błąd", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return new ImportedCourses(suceffulCourses, errorCourses,false);
        }

        async public Task<ImportedCourses> ImportAsync(string path)
        {
            return await Task.Run(()=>Import(path));
        }
    }

}
