﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSReader.ImportModules
{
    public interface IImportModule
    {
         ImportedCourses Import(string path);
         Task<ImportedCourses> ImportAsync(string path);
    }
}
