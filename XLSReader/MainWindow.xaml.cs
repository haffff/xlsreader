﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using XLSReader.ImportModules;

namespace XLSReader
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Settings.Settings.Load();
            InitializeComponent();
            subscribeEvents();
        }

        List<Course> courses;

        private async void importProcessAsync(string path)
        {

            string ext = path.Substring(path.LastIndexOf('.') + 1);

            IImportModule module;

            switch (ext)
            {
                case "xls":
                case "xlsx":
                    module = new XLSImportModule();
                    break;
                case "csv":
                    module = new CSVImportModule();
                    break;
                case "json":
                    module = new JSONImportModule();
                    break;
                default:
                    return;
            }
            this.InfoLabel.Content = "Proszę czekać...";
            var importedCoursesTask = await module.ImportAsync(path);

            if(importedCoursesTask == null)
            {
                return;
            }

            ListView.ItemsSource = importedCoursesTask.SuccessfulCourses;
            courses = importedCoursesTask.SuccessfulCourses;

            var errorWindow = new ErrorCoursesWindow(importedCoursesTask.ErrorCourses);
            errorWindow.Show();

            this.InfoLabel.Content = "Gotowe";
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Title = "Dodaj plik";
            dialog.Filter = "Wszystkie akceptowane pliki|*.xls;*.xlsx;*.csv;*.json|" +
                "Pliki binarne programu Excel (*.xls)|*.xls| Pliki programu Excel  (*.xlsx) |*.xlsx|Pliki CSV (*.csv)|*.csv|Pliki JSON (*.json)|*.json";
            dialog.ShowDialog();
            if (!dialog.CheckPathExists)
            {
                return;
            }

            importProcessAsync(dialog.FileName);
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            Uploader uploader = new Uploader();
            send.IsEnabled = false;
            uploader.UploadAsync(courses, LoginTextBox.Text, PasswordTextBox.SecurePassword, (string message)=> { InfoLabel.Content = message; } );
            send.IsEnabled = true;
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            var window = new SettingsWindow();
            window.Show();
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            
            save.AddExtension = true;
            save.DefaultExt = ".json";
            save.Title = "Wybierz miejsce zapisu";

            if ((bool)save.ShowDialog())
            {
                InfoLabel.Content = "Zapisywanie";
                try
                {
                    string content = JsonConvert.SerializeObject(courses);

                    File.WriteAllText(save.FileName, content);
                    InfoLabel.Content = "Gotowe";
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wystąpił błąd podczas eksportu", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }
    }
}
