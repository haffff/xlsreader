﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace XLSReader
{
    public partial class MainWindow : Window
    {

        private void subscribeEvents()
        {
            ListView.Drop += (sender, e) => {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    string[] x = e.Data.GetData(DataFormats.FileDrop) as string[];
                    if (x.Length > 1)
                    {
                        MessageBox.Show("Program nieobsługuje upuszczania wielu plików", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    else
                    {
                        importProcessAsync(x[0]);
                    }
                }
            };

            ListView.MouseDoubleClick += (sender, e) =>
            {
                if (ListView.SelectedItem != null)
                {
                    Course x = ListView.SelectedItem as Course;
                    var window = new ShowCourseDetails(x);
                    window.Show();
                }
            };

        }
    }
}
