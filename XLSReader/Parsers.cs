﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace XLSReader
{
    static public class Parsers
    {


        public static int ExcelColumnToInt(string index)
        {
            int final = 0;
            int power = index.Length - 1;
            for(int i = 0; i < index.Length; i++)
            {
                int num = (int)(Encoding.ASCII.GetBytes(new char[] { char.ToUpper(index[i]) })[0]) - 64;
                final += num * (int)Math.Pow(26, (double) power);
                power--;
            }
            return --final;
        }

        public static string IntToExcelColumn(int index)
        {
            string final = "";

            index++;
           while(index>0)
            {
                int mod = index % 27;
                index /= 27;
                final += (char)(mod + 64);
            }
            return final;
        }

        public static DateTime SpanDateAndHours(DateTime Date, DateTime Hours)
        {
            Date = Date.AddHours(Hours.Hour);
            Date = Date.AddMinutes(Hours.Minute);

            return Date;
        }

        public static String SecureStringToString(SecureString value)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }






    }
}
