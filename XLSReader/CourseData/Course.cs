﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSReader
{
    public class Course
    {
        public int Id { get; set; }
        public string Driver { get; set; }
        public string Truck { get; set; }
        public int Ramp { get; set; }
        public int Length { get; set; }
        public DateTime PreRouteDate { get; set; }
        public DateTime RouteStartDate { get; set; }
        public DateTime RouteEndDate { get; set; }
        public KeyValuePair<int, DateTime>[] ShopArrivePairs { get; set; }

    }
}
