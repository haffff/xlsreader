﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSReader
{
    public class ImportedCourses
    {
        public ImportedCourses(List<Course> SuccessfulCourses, List<ErrorCourse> ErrorCourses, bool Error)
        {
            this.SuccessfulCourses = SuccessfulCourses;
            this.ErrorCourses = ErrorCourses;
            this.Error = Error;
        }
        public bool Error { get; }
        public List<Course> SuccessfulCourses { get; }
        public List<ErrorCourse> ErrorCourses { get; }
    }
}
