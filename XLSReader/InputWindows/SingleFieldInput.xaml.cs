﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XLSReader.InputWindows
{
    public delegate void InputCallback(object result);
    /// <summary>
    /// Logika interakcji dla klasy SingleFieldInput.xaml
    /// </summary>
    public partial class SingleFieldInput : Window
    {
        public SingleFieldInput(InputCallback callback,string title = "Wprowadź dane")
        {
            InitializeComponent();
            this.callback = callback;
            this.Title = title;
        }
        InputCallback callback;
        public object Result;

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Result = null;
            this.Close();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Result = ValueTextBox.Text;
            callback(Result);
            this.Close();
        }
    }
}
