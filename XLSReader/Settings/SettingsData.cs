﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace XLSReader.Settings
{
    public class SettingsData
    {
        public Dictionary<string, int> Columns { get; set; }
        // Shop Hour
        public Dictionary<int, int> ShopColumnPairs { get; set; }
        public string ServerAdress { get; set; }
        private int addedShops;
    }
}
