﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace XLSReader.Settings
{
    public static class Settings
    {

        public static SettingsData items { get; set; }


        public static bool Load()
        {
            if (File.Exists("config.json"))
            {
                try
                {
                    items = JsonConvert.DeserializeObject<SettingsData>(File.ReadAllText("config.json"));
                    return true;
                }
                catch (Exception e)
                {
                    MessageBoxResult res = MessageBox.Show("Wystąpił błąd podczas ładowania ustawień,czy program ma załadować domyślne ustawienia?(Zalecane)", "Błąd", MessageBoxButton.YesNo, MessageBoxImage.Error);
                    if(res == MessageBoxResult.Yes)
                    {
                        File.Delete("config.json");
                        Load();
                        return true;
                    }
                    else
                        return false;
                }
            }
            else
            {
                items = new SettingsData();
                items.Columns = new Dictionary<string, int>();
                items.ShopColumnPairs = new Dictionary<int, int>();
                items.ServerAdress = "localhost";

                items.Columns.Add("Id", 0);
                items.Columns.Add("Driver", 1);
                items.Columns.Add("Truck", 2);
                items.Columns.Add("Ramp", 3);
                items.Columns.Add("Length", 4);
                items.Columns.Add("Day", 5);
                items.Columns.Add("PreRoute", 7);
                items.Columns.Add("RouteStart", 8);
                items.Columns.Add("RouteEnd", 21);

                items.ShopColumnPairs.Add(10, 13);
                items.ShopColumnPairs.Add(14, 17);
                items.ShopColumnPairs.Add(18, 21);
                Save();
                return true;
            }
        }

        public static bool Save()
        {
            try
            {
                File.WriteAllText("config.json",JsonConvert.SerializeObject(items));
                return true;
            }
            catch(Exception e)
            {
                MessageBox.Show("Wystąpił błąd podczas zapisu.","Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
                return false;
            }
        }



    }
}
