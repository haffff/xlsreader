﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Windows;


namespace XLSReader
{
    public delegate void Inform(string message);

    public class Uploader
    {
        //To check
        async Task<string> getTokenAsync(string user, SecureString password)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            Dictionary<string, string> userContent = new Dictionary<string, string>();
            userContent.Add("login", user);
            userContent.Add("password", Parsers.SecureStringToString(password));

            HttpClient client = new HttpClient();
            try
            {
                var response = await client.PostAsync(Settings.Settings.items.ServerAdress + "/api/getToken.php", new FormUrlEncodedContent(userContent));


                    return await response.Content.ReadAsStringAsync();

            }
            catch(WebException e)
            {
                MessageBox.Show("Nie można połączyć się z serwerem. Sprawdź swoje połaczenie i Czy adres url w ustawieniach jest dobrze wpisany.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            catch(Exception e)
            {
                MessageBox.Show("Wystąpił nieoczekiwany błąd", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        async Task<string> uploadJsonAsync(string json, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;


            HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.PostAsync(Settings.Settings.items.ServerAdress + "/api/uploadContent.php", new StringContent(json));
            return await response.Content.ReadAsStringAsync();
        }
        

        public async void UploadAsync(List<Course> courses , string user, SecureString password, Inform inform )
        {
            inform("Logowanie...");
            string json = await getTokenAsync(user, password);
            if(json == null)
            {
                inform("Podczas logowania wystąpił błąd");
                return;
            }

            Dictionary<string,string> result;

            try
            {
                result = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }catch(Newtonsoft.Json.JsonReaderException e)
            {
                MessageBox.Show("Wystąpił błąd przy odbiorze informacji zwrotnej. Czy jesteś pewien że dobrze wpisałeś adres serwera w ustawieniach?","Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
                inform("Podczas logowania wystąpił błąd");
                return;
            }

            if(result["success"] == "false")
            {
                inform(result["message"]);
                return;
            }

            inform("Przygotowywanie danych do wysyłania...");

            //List<List<Course>> separatedCourses = new List<List<Course>>();
            //int i = 1;

            //List<Course> list = new List<Course>();

            //foreach(Course x in courses)
            //{
            //    list.Add(x);

            //    if(i%20 == 0)
            //    {
            //        separatedCourses.Add(list);
            //        list = new List<Course>();
            //    }
            //}
            //separatedCourses.Add(list);



            string jsonContent = JsonConvert.SerializeObject(courses);



            inform("Wysyłanie...");

            var sendResult = await uploadJsonAsync(jsonContent, result["jwt"]);
            
            var decodedSendResult = JsonConvert.DeserializeObject<Dictionary<string,string>>(sendResult);
            if(decodedSendResult["success"] == "false")
            {
                MessageBox.Show("Pojawił się problem podczas przetwarzania odbieranych danych (Token jest nieprawidłowy?)","Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            else
            {
                inform("Gotowe");
            }

            return;
        }







    }
}
