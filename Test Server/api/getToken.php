<?php
require_once '../vendor/autoload.php';
require_once '../config.php';
require "../DbManagment.php";

use \Firebase\JWT\JWT;


header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$databaseService = new DataBase();
$databaseService->Connect();


$login = $_POST["login"];
$password = $_POST["password"];

$table_name = 'Users';

$query = "SELECT login,password,id FROM users WHERE login = '$login'";

$databaseService->Query($query);
$array = $databaseService->getResultAsArray();

if(!Empty($array[0])){
    $dblogin = $array[0][0];
    $dbpasswd = $array[0][1];
    $id = $array[0][2];
    if(password_verify( $password, $dbpasswd))
    {
        $secret_key = "749C2F83C8352680C9997C74C7B07F4833C2CC24729BB979E58C834D7A73A75B";
        $issuer_claim = "http://localhost"; // this can be the servername
        $audience_claim = "http://localhost.pl";
        $issuedat_claim = time(); // issued at
        $expire_claim = $issuedat_claim + 60; // expire time in seconds
        $token = array(
            "iss" => $issuer_claim,
            "aud" => $audience_claim,
            "iat" => $issuedat_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => $id
        ));

        http_response_code(200);
        
        $jwt = JWT::encode($token, $SECRETKEY);
        echo json_encode(
            array(
                "message" => "Pomyślnie zalogowano.",
                "success" => "true",
                "jwt" => $jwt,
                "expireAt" => $expire_claim
            ));
    }
    else{
        http_response_code(401);
        echo json_encode(array("message" => "Złe hasło.", "success"=>"false"));
    }
}
else
{
    echo json_encode(array("message" => "Nie ma takiego użytkownika", "success" => "false"));
}
?>