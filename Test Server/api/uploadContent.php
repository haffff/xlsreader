<?php
require_once '../vendor/autoload.php';
require_once '../config.php';
require "../DbManagment.php";

use \Firebase\JWT\JWT;
use Zend\Http\PhpEnvironment\Request;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$databaseService = new DataBase();
$databaseService->Connect();

$request = new Request();

    $auth = $request->getHeader('authorization');
    
    if($auth)
    {
        list($jwt) = sscanf($auth->toString(), 'Authorization: Bearer %s');
        if($jwt)
        {
            try{
                JWT::decode($jwt,$SECRETKEY,array('HS256'));
                $data = json_decode(file_get_contents('php://input'));
                $data = array($data);
                //echo var_dump($data);
                //TODO sending to mysql
                echo json_encode(array("success" => "true"));
                return;

            }catch (Exception $e){
                
                echo json_encode(array("success" => "false"));
                return;
            }
        }
    
}

 echo json_encode(array("success" => "false"));